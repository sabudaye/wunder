defmodule Wunder.BoundingBox do
  @moduledoc """
  BoundingBox context: Struct definition,context functions
  """
  alias Wunder.Coordinates
  alias Wunder.BoundingBox

  @enforce_keys [:min_lon, :min_lat, :max_lon, :max_lat]

  defstruct [:min_lon, :min_lat, :max_lon, :max_lat]

  @type t :: %Wunder.BoundingBox{
          min_lon: number(),
          min_lat: number(),
          max_lon: number(),
          max_lat: number()
        }

  @spec create([tuple()]) :: %BoundingBox{}
  @doc """
  Create BoundingBox from pair(list) of coordinates

  ## Examples

      iex> Wunder.BoundingBox.create([
      ...> {120.99287, 14.75659},
      ...> {120.99206, 14.756699999999999}
      ...> ])
      %BoundingBox{
        min_lon: 120.99206,
        min_lat: 14.75659,
        max_lon: 120.99287,
        max_lat: 14.756699999999999
      }
  """
  def create([point1, point2]) do
    create(Coordinates.create(point1), Coordinates.create(point2))
  end

  @doc """
  Create BoundingBox from two coordinate points

  ## Examples

      iex> Wunder.BoundingBox.create(
      ...> %Coordinates{lon: 120.99287, lat: 14.75659},
      ...> %Coordinates{lon: 120.99206, lat: 14.756699999999999}
      ...> )
      %BoundingBox{
        min_lon: 120.99206,
        min_lat: 14.75659,
        max_lon: 120.99287,
        max_lat: 14.756699999999999
      }
  """
  @spec create(
          %Coordinates{},
          %Coordinates{}
        ) :: %BoundingBox{}
  def create(point1, point2) do
    %BoundingBox{
      min_lon: min(point1.lon, point2.lon),
      min_lat: min(point1.lat, point2.lat),
      max_lon: max(point1.lon, point2.lon),
      max_lat: max(point1.lat, point2.lat)
    }
  end

  @spec create(
          %BoundingBox{},
          %Coordinates{}
        ) :: true | false
  @doc """
  Create BoundingBox from two coordinate points

  ## Examples

      iex> Wunder.BoundingBox.inside?(
      ...> %BoundingBox{
      ...>   min_lon: 120.99206,
      ...>   min_lat: 14.75659,
      ...>   max_lon: 120.99287,
      ...>   max_lat: 14.756699999999999},
      ...> %Coordinates{lon: 120.99206, lat: 14.756699999999999}
      ...> )
      true
  """
  def inside?(bounding_box, %Coordinates{lon: lon, lat: lat}) do
    with true <- bounding_box.min_lon <= lon,
         true <- bounding_box.max_lon >= lon,
         true <- bounding_box.min_lat <= lat,
         true <- bounding_box.max_lat >= lat do
      true
    else
      _ -> false
    end
  end
end
