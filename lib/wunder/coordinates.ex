defmodule Wunder.Coordinates do
  @moduledoc """
  Coordinates context: Struct definition, context functions
  """
  alias Wunder.Coordinates

  @enforce_keys [:lon, :lat]

  defstruct [:lon, :lat]

  @type t :: %Wunder.Coordinates{
          lon: number(),
          lat: number()
        }

  @spec create(tuple() | list()) :: %Coordinates{}
  @doc """
  create %Coordinates{} struct from tuple or list of longitude and latitute.

  ## Examples

      iex> Wunder.Coordinates.create({120.99287, 14.75659})
      %Coordinates{lon: 120.99287, lat: 14.75659}

      iex> Wunder.Coordinates.create([120.99287, 14.75659])
      %Coordinates{lon: 120.99287, lat: 14.75659}
  """
  def create({lon, lat}), do: %Coordinates{lon: lon, lat: lat}
  def create([lon, lat]), do: %Coordinates{lon: lon, lat: lat}
  def create(%Coordinates{} = coordinates), do: coordinates

  @spec create(number(), number()) :: %Coordinates{}
  @doc """
  create %Coordinates{} struct from longitude and latitute.

  ## Examples

      iex> Wunder.Coordinates.create(120.99287, 14.75659)
      %Coordinates{lon: 120.99287, lat: 14.75659}
  """
  def create(lon, lat), do: %Coordinates{lon: lon, lat: lat}

  @spec valid?(%Coordinates{}) :: true | false
  @doc """
  Coordinates validation

  ## Examples

      iex> Wunder.Coordinates.valid?(%Coordinates{lon: 120.99287, lat: 14.75659})
      true

      iex> Wunder.Coordinates.valid?(%Coordinates{lon: 190.99287, lat: 14.75659})
      false
  """
  def valid?(%Coordinates{lon: lon, lat: lat}) do
    with true <- lon >= -180,
         true <- lon <= 180,
         true <- lat >= -90,
         true <- lat <= 90 do
      true
    else
      _ -> false
    end
  end

  @spec coordinates_pairs(Enum.t()) :: [Enum.t()]
  @doc """
  Create lis of %Coordinates pairs from %Coordinates{} list

  ## Examples

      iex> [
      ...> %Coordinates{lon: 120.99287, lat: 14.75659},
      ...> %Coordinates{lon: 120.99206, lat: 14.756699999999999},
      ...> %Coordinates{lon: 120.99203999999999, lat: 14.756939999999998}
      ...> ]|> Wunder.Coordinates.coordinates_pairs
      [
        [
          %Coordinates{lon: 120.99287, lat: 14.75659},
          %Coordinates{lon: 120.99206, lat: 14.756699999999999}
        ],
        [
          %Coordinates{lon: 120.99206, lat: 14.756699999999999},
          %Coordinates{lon: 120.99203999999999, lat: 14.756939999999998}
        ]
      ]
  """
  def coordinates_pairs(pairs) do
    pairs
    |> Enum.zip(Enum.slice(pairs, 1..-1))
    |> Enum.map(&Tuple.to_list/1)
  end
end
