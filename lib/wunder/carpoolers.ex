defmodule Wunder.Carpoolers do
  @moduledoc """
  Carpoolers context
  """

  use Agent
  alias Wunder.BoundingBox
  alias Wunder.Coordinates

  @pairs_file_path "files/pairs.csv"

  def start_link(_) do
    bounidng_boxes = Wunder.create_bounding_boxes(@pairs_file_path)
    Agent.start_link(fn -> bounidng_boxes end, name: __MODULE__)
  end

  @spec get_all :: list()
  @doc """
  Get all carpolers's bounding boxes
  """
  def get_all() do
    Agent.get(__MODULE__, fn state ->
      state
    end)
  end

  @spec get_by_waypoints([%Coordinates{}]) :: list()
  @doc """
  Get carpolers's bounding boxes by waypoints list
  """
  def get_by_waypoints(waypoints) do
    Agent.get(__MODULE__, fn state ->
      Enum.map(waypoints, fn wp ->
        {
          wp,
          Enum.find(state, fn bb ->
            BoundingBox.inside?(bb, wp)
          end)
        }
      end)
    end)
  end

  @spec store(%Coordinates{}, %Coordinates{}) :: :ok
  @doc """
  Build BoundingBox from coordinates pair and store to state
  """
  def store(point1, point2) do
    bounding_box = BoundingBox.create(point1, point2)
    Agent.update(__MODULE__, fn state -> [bounding_box | state] end)
  end
end
