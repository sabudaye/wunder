defmodule Wunder.CsvFileUtils do
  @moduledoc """
  Csv Utils module, for DRY
  """

  alias Wunder.Coordinates

  @spec parse_csv(String.t()) :: [Enum.t()]
  @doc """
  Parse lon lat csv file to array of %Coordinates{}.

  ## Examples

      iex> Wunder.CsvFileUtils.parse_csv("files/test_pairs.csv")
      [
        %Coordinates{lon: 120.99287, lat: 14.75659},
        %Coordinates{lon: 120.99206, lat: 14.756699999999999},
        %Coordinates{lon: 120.99203999999999, lat: 14.756939999999998},
        %Coordinates{lon: 120.99207999999999, lat: 14.757139999999998}
      ]
  """
  def parse_csv(path) do
    path
    |> File.stream!()
    |> CSV.decode(separator: ?,, headers: true)
    |> Stream.map(&to_float_pair/1)
    |> Stream.map(&Coordinates.create/1)
    |> Stream.filter(&Coordinates.valid?/1)
    |> Enum.to_list()
  end

  @spec to_float_pair(
          {:ok, [%{required(String.t()) => String.t(), required(String.t()) => String.t()}]}
        ) :: {float(), float()}
  @doc """
  Tuple of two floats from result of CSV decode

  ## Examples

      iex> Wunder.CsvFileUtils.to_float_pair({:ok, %{"lon" => 120.99287, "lat" => 14.75659}})
      {120.99287, 14.75659}
  """
  def to_float_pair({:ok, pair}) do
    {to_float(pair["lon"]), to_float(pair["lat"])}
  end

  defp to_float(value) when is_binary(value), do: String.to_float(value)
  defp to_float(value), do: value
end
