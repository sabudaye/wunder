defmodule Wunder.Application do
  @moduledoc false
  use Application

  @spec start(term, term) :: {:error, term} | {:ok, pid} | {:ok, pid, term}
  def start(_type, _args) do
    children = [
      Wunder.Carpoolers.child_spec(restart: :one_for_one)
    ]

    opts = [strategy: :one_for_one, name: Wunder.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
