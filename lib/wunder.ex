defmodule Wunder do
  @moduledoc """
  Wunder test task
  """

  alias Wunder.CsvFileUtils
  alias Wunder.Coordinates
  alias Wunder.BoundingBox

  @spec create_bounding_boxes(String.t()) :: [%BoundingBox{}]
  @doc """
  Creates list of %BoundingBox{} from pairs.csv file fromat (lon, lat).

  ## Examples

      iex> Wunder.create_bounding_boxes("files/test_pairs.csv")
      [
        %Wunder.BoundingBox{
          max_lat: 14.756699999999999,
          max_lon: 120.99287,
          min_lat: 14.75659,
          min_lon: 120.99206
        },
        %Wunder.BoundingBox{
          max_lat: 14.756939999999998,
          max_lon: 120.99206,
          min_lat: 14.756699999999999,
          min_lon: 120.99203999999999
        },
        %Wunder.BoundingBox{
          max_lat: 14.757139999999998,
          max_lon: 120.99207999999999,
          min_lat: 14.756939999999998,
          min_lon: 120.99203999999999
        }
      ]
  """
  def create_bounding_boxes(path) do
    path
    |> CsvFileUtils.parse_csv()
    |> Coordinates.coordinates_pairs()
    |> Enum.map(&BoundingBox.create/1)
  end

  @spec filter_coordinates([%BoundingBox{}], String.t()) :: [%BoundingBox{}]
  @doc """
  Filter coordinates.csv by the given bounding boxes
  """
  def filter_coordinates(bounding_boxes, path) do
    path
    |> CsvFileUtils.parse_csv()
    |> Enum.filter(fn coordinates ->
      Enum.any?(bounding_boxes, fn bb ->
        BoundingBox.inside?(bb, coordinates)
      end)
    end)
  end
end
