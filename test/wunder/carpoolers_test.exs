defmodule Wunder.CarpoolersTest do
  use ExUnit.Case

  alias Wunder.Carpoolers
  alias Wunder.BoundingBox
  alias Wunder.Coordinates

  test "gets state from carpoolers repo" do
    carpoolers = Carpoolers.get_all()

    assert Enum.count(carpoolers) == 327

    assert Enum.take(carpoolers, 1) == [
             %BoundingBox{
               max_lat: 14.756699999999999,
               max_lon: 120.99287,
               min_lat: 14.75659,
               min_lon: 120.99206
             }
           ]
  end

  test "returns bounding boxes for waypoints" do
    result = Carpoolers.get_by_waypoints([%Coordinates{lon: 120.99287, lat: 14.75659}])

    assert result == [
             {%Coordinates{lat: 14.75659, lon: 120.99287},
              %BoundingBox{
                max_lat: 14.756699999999999,
                max_lon: 120.99287,
                min_lat: 14.75659,
                min_lon: 120.99206
              }}
           ]

    result2 = Carpoolers.get_by_waypoints([%Coordinates{lon: 120.99287, lat: 90}])

    assert result2 == [
             {%Coordinates{lat: 90, lon: 120.99287}, nil}
           ]

    result3 =
      Carpoolers.get_by_waypoints([
        %Coordinates{lon: 120.99287, lat: 14.75659},
        %Coordinates{lon: 120.99203999999999, lat: 14.756939999999998}
      ])

    assert result3 == [
             {%Wunder.Coordinates{lon: 120.99287, lat: 14.75659},
              %Wunder.BoundingBox{
                max_lat: 14.756699999999999,
                max_lon: 120.99287,
                min_lat: 14.75659,
                min_lon: 120.99206
              }},
             {%Wunder.Coordinates{lat: 14.756939999999998, lon: 120.99203999999999},
              %Wunder.BoundingBox{
                max_lat: 14.756939999999998,
                max_lon: 120.99206,
                min_lat: 14.756699999999999,
                min_lon: 120.99203999999999
              }}
           ]
  end

  test "creates and stores new bounding box from pair of coordinates" do
    Carpoolers.store(%Coordinates{lon: 1, lat: 1}, %Coordinates{lon: 2, lat: 2})

    result =
      Carpoolers.get_by_waypoints([
        %Coordinates{lon: 2, lat: 2},
        %Coordinates{lon: 1, lat: 1}
      ])

    assert result == [
             {%Wunder.Coordinates{lon: 2, lat: 2},
              %Wunder.BoundingBox{
                max_lat: 2,
                max_lon: 2,
                min_lat: 1,
                min_lon: 1
              }},
             {%Wunder.Coordinates{lat: 1, lon: 1},
              %Wunder.BoundingBox{
                max_lat: 2,
                max_lon: 2,
                min_lat: 1,
                min_lon: 1
              }}
           ]
  end
end
