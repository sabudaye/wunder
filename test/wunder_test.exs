defmodule WunderTest do
  use ExUnit.Case

  doctest Wunder

  test "returns list of bounding boxes" do
    result =
      "files/pairs.csv"
      |> Wunder.create_bounding_boxes()
      |> Wunder.filter_coordinates("files/coordinates.csv")

    assert Enum.count(result) == 10
  end
end
